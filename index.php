<?php
trait Hewan
{
    use Fight;
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo "<h3>Aksi</h3>";
        return $this->nama . " sedang " . $this->keahlian . '<br>';
    }
}

trait Fight
{
    // use Hewan;
    public $attackPower = 10;
    public $deffencePower = 5;

    public function serang($obj)
    {
        echo "<h3>Serangan</h3>";
        echo $this->nama . " sedang menyerang " . $obj;
        return;
    }
    public function diserang($obj, $attack_damage)
    {
        $damage = $attack_damage / $this->deffencePower;
        $sisa_darah = $this->darah - ($damage);
        $this->darah = $sisa_darah;

        echo "<h3>Diserang</h3>";
        echo $this->nama . " sedang diserang " . $obj . "<br> total serangan sebesar " . $damage . "<br>";
        echo "darah " . $this->nama . " tersisa " . $sisa_darah;
        return;
    }
}


class Elang
{
    use Hewan;
    use Fight;

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $deffencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->deffencePower = $deffencePower;
    }

    public function getInfoHewan()
    {
        echo "<h4>Info Hewan</h4>";
        echo "Nama: " . $this->nama . '<br>';
        echo "Darah: " . $this->darah . '<br>';
        echo "Jumlah Kaki: " . $this->jumlahKaki . '<br>';
        echo "Keahlian: " . $this->keahlian . '<br>';
        echo "Attack Power: " . $this->attackPower . '<br>';
        echo "Deffence Power: " . $this->deffencePower . '<br>';
    }
    public function get_nama()
    {
        return $this->nama;
    }
    public function get_attackPower()
    {
        return $this->attackPower;
    }
}

class Harimau
{
    use Hewan;
    use Fight;

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $deffencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->deffencePower = $deffencePower;
    }

    public function getInfoHewan()
    {
        echo "<h4>Info Hewan</h4>";
        echo "Nama: " . $this->nama . '<br>';
        echo "Darah: " . $this->darah . '<br>';
        echo "Jumlah Kaki: " . $this->jumlahKaki . '<br>';
        echo "Keahlian: " . $this->keahlian . '<br>';
        echo "Attack Power: " . $this->attackPower . '<br>';
        echo "Deffence Power: " . $this->deffencePower . '<br>';

        echo '<br><br><br>';
    }

    public function get_nama()
    {
        return $this->nama;
    }
    public function get_attackPower()
    {
        return $this->attackPower;
    }
}

$elang = new Elang("elang_1", 2, "Terbang tinggi", 10, 5);
$harimau = new Harimau("harimau_1", 4, "Lari cepat", 7, 8);
$nama_harimau = $harimau->nama;
$attack_harimau = $harimau->get_attackPower();
$nama_elang = $elang->nama;
$attack_elang = $elang->get_attackPower();

echo "<h1>Elang</h1>";
echo $elang->getInfoHewan();
echo $elang->atraksi();
echo $elang->serang($nama_harimau);
echo $elang->diserang($nama_harimau, $attack_harimau);
echo $elang->getInfoHewan();


echo "<h1>Harimau</h1>";
echo $harimau->getInfoHewan();
echo $harimau->atraksi();
echo $harimau->serang($nama_elang);
echo $harimau->diserang($nama_elang, $attack_elang);
echo $harimau->getInfoHewan();
